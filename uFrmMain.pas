unit uFrmMain;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   System.IOUtils,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.ScrollBox,
   FMX.Memo,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.Edit,
   FMX.Layouts,
   System.Actions,
   FMX.ActnList,
   FMX.StdActns,
   FMX.MediaLibrary.Actions,
   FMX.Platform,
   IdBaseComponent,
   IdComponent,
   IdIOHandler,
   IdIOHandlerSocket,
   IdIOHandlerStack,
   IdSSL,
   IdSSLOpenSSL,
   IdSSLOpenSSLHeaders;

type
   TFrmMain = class(TForm)
      navBar: TToolBar;
      lblTitle: TLabel;
      mmLog: TMemo;
      btnBusca: TButton;
      lytCEP: TLayout;
      edtCEP: TEdit;
      btnCompartilha: TButton;
      actlMain: TActionList;
      actShare: TShowShareSheetAction;
      openSSL: TIdSSLIOHandlerSocketOpenSSL;
      btnLimpaCEP: TButton;
      procedure btnBuscaClick(Sender: TObject);
      procedure edtCEPKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
      procedure actShareBeforeExecute(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure btnLimpaCEPClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

uses
   WebServiceCorreios;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
   IdOpenSSLSetLibPath(System.IOUtils.TPath.GetDocumentsPath);
end;

procedure TFrmMain.btnLimpaCEPClick(Sender: TObject);
begin
   edtCEP.text := EmptyStr;
   edtCEP.SetFocus;
end;

procedure TFrmMain.edtCEPKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
   if (Key = vkReturn) then
   begin
      btnBuscaClick(Sender);
      Key := vkNone;
   end;
end;

procedure TFrmMain.btnBuscaClick(Sender: TObject);
var
   LSyncService: IFMXDialogServiceASync;

   strCEP: string;
   resultCEP: enderecoERP;
   tmr1, tmr2, tmr3: TDateTime;
begin
   TButton(Sender).Enabled := False;
   try
      tmr1 := Now;

      mmLog.lines.Clear;

      strCEP := edtCEP.text;

      if (strCEP = EmptyStr) then
      begin
         if TPlatformServices.Current.SupportsPlatformService( //
            IFMXDialogServiceASync, IInterface(LSyncService)) then
         begin
            LSyncService.MessageDialogAsync('CEP em branco!' //
               , TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0,
               procedure(const AResult: TModalResult)
               begin
                  case AResult of
                     mrOk:
                        begin
                           edtCEP.SetFocus;
                        end;
                  end;
               end);
         end;
         Exit;
      end;

      if not(Length(strCEP) = 8) then
      begin
         if TPlatformServices.Current.SupportsPlatformService( //
            IFMXDialogServiceASync, IInterface(LSyncService)) then
         begin
            LSyncService.MessageDialogAsync('Favor digitar o CEP com 8 numeros!' //
               , TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0,
               procedure(const AResult: TModalResult)
               begin
                  case AResult of
                     mrOk:
                        begin
                           edtCEP.SetFocus;
                        end;
                  end;
               end);
         end;
         Exit;
      end;

      if not(strCEP = EmptyStr) and (Length(strCEP) = 8) then
      begin
         try
            resultCEP := GetAtendeCliente.consultaCEP(edtCEP.text);
            try
               mmLog.lines.Add('CEP: ' + resultCEP.cep);
               mmLog.lines.Add('Logradouro: ' + resultCEP.end_);
               mmLog.lines.Add('Complemento 1: ' + resultCEP.complemento);
               mmLog.lines.Add('Complemento 2: ' + resultCEP.complemento2);
               mmLog.lines.Add('Bairro: ' + resultCEP.bairro);
               mmLog.lines.Add('Cidade: ' + resultCEP.cidade);
               mmLog.lines.Add('UF: ' + resultCEP.uf);
            finally
               {$IFDEF MSWINDOWS}
               FreeAndNil(resultCEP);
               {$ENDIF}
               {$IF DEFINED(iOS) or DEFINED(ANDROID)}
               resultCEP.DisposeOf;
               {$ENDIF}
            end;
         except
            on E: Exception do
            begin
               raise Exception.Create('� necess�rio conex�o com a internet!');
               // if TPlatformServices.Current.SupportsPlatformService( //
               // IFMXDialogServiceASync, IInterface(LSyncService)) then
               // begin
               // LSyncService.MessageDialogAsync('Erro: ' + E.ClassName + '. ' + E.Message //
               // , TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0,
               // procedure(const AResult: TModalResult)
               // begin
               // case AResult of
               // mrOk:
               // begin
               // Exit;
               // end;
               // end;
               // end);
            end;
         end;
      end;

      tmr2 := Now;
      tmr3 := tmr2 - tmr1;

      mmLog.lines.Add('Processou em ' + FormatDateTime('hh:mm:ss:zzz', tmr3));

   finally
      TButton(Sender).Enabled := True;
   end;
end;

procedure TFrmMain.actShareBeforeExecute(Sender: TObject);
var
   strLog: string;
begin
   strLog := mmLog.lines.text;

   if not(strLog = EmptyStr) then
      actShare.TextMessage := mmLog.lines.text;
end;

end.
